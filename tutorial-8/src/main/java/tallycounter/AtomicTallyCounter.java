package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter extends TallyCounter {

    private AtomicInteger atInt = new AtomicInteger();

    public void increment() {
        atInt.getAndIncrement();
    }

    public void decrement() {
        atInt.getAndDecrement();
    }

    public int value() {
        return atInt.intValue();
    }
}