package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    public static  int[] quickSort(int[] arrInt) {
        quickSort(arrInt, 0, arrInt.length - 1);
        return arrInt;
    }

    /**
     * Sorting algorithm, with array of interger as
     * a paramater need to be sorted.
     * and return as sorted array
     */

    public static void quickSort(int[] arrInt, int s, int e) {
        int start = s;
        int end = e;
        int pivot = arrInt[s + (e - s) / 2];

        while (start <= end) {
            while (start < e && arrInt[start] < pivot) {
                start++;
            }
            while (end > s && arrInt[end] > pivot) {
                end--;
            }
            if (start <= end) {
                swap(arrInt, start, end);
                start++;
                end--;
            }
        }

        swap(arrInt, start, e);
        if (s < end) {
            quickSort(arrInt, s, end);
        }
        if (start < e) {
            quickSort(arrInt, start, end);
        }
    }


    private static void swap(int[] arrInt, int val1, int val2) {
        int tmp = arrInt[val1];
        arrInt[val1] = arrInt[val2];
        arrInt[val2] = tmp;
    }


}
