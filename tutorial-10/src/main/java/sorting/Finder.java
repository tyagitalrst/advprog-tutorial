package sorting;

public class Finder {


    /**
     * Some searching algorithm that possibly the slowest algorithm.
     * This algorithm can search a value irregardless of whether the sequence already sorted or not.
     * @param arrOfInt is a sequence of integer.
     * @param searchedValue value that need to be searched inside the sequence.
     * @return -1 if there are no such value inside the sequence, else return searchedValue.
     */

    public static int slowSearch(int[] arrOfInt, int searchedValue) {
        int returnValue = -1;

        for (int element : arrOfInt) {
            if (element == searchedValue) {
                returnValue = element;
            }
        }

        return returnValue;
    }

    /**
     * Method will return -1 if there are no such value inside the sequence.
     * and else will return searchedValue.
     * @param arrInt is a sequence of integer.
     * @param searchInt value that need to be searched inside the sequence.
     */

    public  static  int quickSearch(int[] arrInt, int searchInt) {
        for (int elm : arrInt) {
            if (elm == searchInt) {
                return elm;
            }
        }

        return -1;
    }

    /**
     * Method dived array into 2 sub array.
     * check the middle value of the array, to decide which sub array should be check.
     * and do recursicely, until found the searchedValue.
     * Method will return -1 if there are no such value inside the sequence.
     * and else will return searchedValue.
     * @param arrInt is a sequence of integer.
     * @param searchInt value that need to be searched inside the sequence.
     */

    public static int binSearch(int[] arrInt, int searchInt) {
        return binSearchHelper(arrInt, searchInt, 0, arrInt.length - 1);
    }

    private static int binSearchHelper(int[] arrInt, int searchInt, int start, int end) {
        if (end >= 1) {
            int mid = start + (end - start) / 2;

            if (arrInt[mid] == searchInt) {
                return searchInt;
            } else if (arrInt[mid] > searchInt) {
                return binSearchHelper(arrInt, searchInt, start, mid - 1);
            }

            return binSearchHelper(arrInt, searchInt, mid + 1, end);
        }
        return  -1;
    }
}
