package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {
    @GetMapping("/cv")
    public String cv(@RequestParam(name = "vstr", required = false)
                             String vstr, Model model) {
        if (vstr == null || vstr.equals("")) {
            model.addAttribute("title", "This is my CV");
        } else {
            model.addAttribute("title", vstr + ", I hope you interested to hire me");
        }
        return "cv";
    }

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

}
