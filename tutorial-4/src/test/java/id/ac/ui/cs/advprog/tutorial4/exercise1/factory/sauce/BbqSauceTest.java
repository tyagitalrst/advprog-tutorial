package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BbqSauce;
import org.junit.Before;
import org.junit.Test;

public class BbqSauceTest {
	private BbqSauce bbqSauce;

	@Before
	public void setUp() {
		bbqSauce = new BbqSauce();
	}

	@Test
    public void testToString() {
    	assertEquals("BBQ Sauce sauce with beef", bbqSauce.toString());
    }
}