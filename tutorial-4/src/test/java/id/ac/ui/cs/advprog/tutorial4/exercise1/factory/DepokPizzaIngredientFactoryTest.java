package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {
	DepokPizzaIngredientFactory depokPizzaIngredientFactory;

	@Before
	public void setUp() {
		depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
	}

    @Test
    public void createDoughTest() {
    	assertEquals(depokPizzaIngredientFactory.createDough().toString(), "ThickCrust style extra thick crust dough");
    }

    @Test
    public void createSauceTest() {
    	assertEquals(depokPizzaIngredientFactory.createSauce().toString(), "Tomato sauce with plum tomatoes");
    }

    @Test
    public void createCheeseTest() {
    	assertEquals(depokPizzaIngredientFactory.createCheese().toString(), "Shredded Mozzarella");
    }

    @Test
    public void createClamTest() {
    	assertEquals(depokPizzaIngredientFactory.createClam().toString(), "Fresh Clams from Long Island Sound");
    }
}