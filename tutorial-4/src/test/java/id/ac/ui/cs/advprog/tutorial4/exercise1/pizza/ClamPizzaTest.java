package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;

import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;

public class ClamPizzaTest {
	private ClamPizza depokClamPizza;
    private ClamPizza newYorkClamPizza;

	@Before
	public void setUp() {
		depokClamPizza = new ClamPizza(new DepokPizzaIngredientFactory());
        newYorkClamPizza = new ClamPizza(new NewYorkPizzaIngredientFactory());
	}

    @Test
    public void prepareDepokClamPizzaTest() {
        depokClamPizza.prepare();
        
    	assertEquals(depokClamPizza.dough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(depokClamPizza.sauce.toString(), "Tomato sauce with plum tomatoes");
        assertEquals(depokClamPizza.clam.toString(), "Fresh Clams from Long Island Sound");
    }

    @Test
    public void prepareNewYorkClamPizzaTest() {
        newYorkClamPizza.prepare();
        
        assertEquals(newYorkClamPizza.dough.toString(), "Thin Crust Dough");
        assertEquals(newYorkClamPizza.sauce.toString(), "Marinara Sauce");
        assertEquals(newYorkClamPizza.clam.toString(), "Fresh Clams from Long Island Sound");
    }



}