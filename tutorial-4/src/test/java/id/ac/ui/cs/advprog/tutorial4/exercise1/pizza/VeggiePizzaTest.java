package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;

import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;

public class VeggiePizzaTest {
	private VeggiePizza depokVeggiePizza;
    private VeggiePizza newYorkVeggiePizza;

	@Before
	public void setUp() {
		depokVeggiePizza = new VeggiePizza(new DepokPizzaIngredientFactory());
        newYorkVeggiePizza = new VeggiePizza(new NewYorkPizzaIngredientFactory());
	}


    @Test
    public void prepareDepokCheesePizzaTest() {
        depokVeggiePizza.prepare();
        
        assertEquals(depokVeggiePizza.dough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(depokVeggiePizza.sauce.toString(), "Tomato sauce with plum tomatoes");
        assertEquals(depokVeggiePizza.cheese.toString(), "Shredded Mozzarella");
    }

    @Test
    public void prepareNewYorkCheesePizzaTest() {
        newYorkVeggiePizza.prepare();
        
        assertEquals(newYorkVeggiePizza.dough.toString(), "Thin Crust Dough");
        assertEquals(newYorkVeggiePizza.sauce.toString(), "Marinara Sauce");
        assertEquals(newYorkVeggiePizza.cheese.toString(), "Reggiano Cheese");
    }





}