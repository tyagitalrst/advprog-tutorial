package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;

import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;

public class CheesePizzaTest {
	private CheesePizza depokCheesePizza;
    private CheesePizza newYorkCheesePizza;

	@Before
	public void setUp() {
		depokCheesePizza = new CheesePizza(new DepokPizzaIngredientFactory());
        newYorkCheesePizza = new CheesePizza(new NewYorkPizzaIngredientFactory());
	}

    @Test
    public void prepareDepokCheesePizzaTest() {
        depokCheesePizza.prepare();
        
    	assertEquals(depokCheesePizza.dough.toString(), "ThickCrust style extra thick crust dough");
        assertEquals(depokCheesePizza.sauce.toString(), "Tomato sauce with plum tomatoes");
        assertEquals(depokCheesePizza.cheese.toString(), "Shredded Mozzarella");
    }

    @Test
    public void prepareNewYorkCheesePizzaTest() {
        newYorkCheesePizza.prepare();
        
        assertEquals(newYorkCheesePizza.dough.toString(), "Thin Crust Dough");
        assertEquals(newYorkCheesePizza.sauce.toString(), "Marinara Sauce");
        assertEquals(newYorkCheesePizza.cheese.toString(), "Reggiano Cheese");
    }



}