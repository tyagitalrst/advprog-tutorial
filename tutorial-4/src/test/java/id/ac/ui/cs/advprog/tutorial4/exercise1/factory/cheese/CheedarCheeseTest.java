package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.CheedarCheese;
import org.junit.Before;
import org.junit.Test;

public class CheedarCheeseTest {
	private CheedarCheese cheedarCheese;

	@Before
	public void setUp() {
		cheedarCheese = new CheedarCheese();
	}


	@Test
    public void testToString() {
    	assertEquals("Cheedar Cheese", cheedarCheese.toString());
    }
}