package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;


import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {
	NewYorkPizzaIngredientFactory newYorkPizzaIngredientFactory;

	@Before
	public void setUp() {
		newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
	}

    @Test
    public void createDoughTest() {
    	assertEquals(newYorkPizzaIngredientFactory.createDough().toString(), "Thin Crust Dough");
    }

    @Test
    public void createSauceTest() {
    	assertEquals(newYorkPizzaIngredientFactory.createSauce().toString(), "Marinara Sauce");
    }

    @Test
    public void createCheeseTest() {
    	assertEquals(newYorkPizzaIngredientFactory.createCheese().toString(), "Reggiano Cheese");
    }

    @Test
    public void createClamTest() {
    	assertEquals(newYorkPizzaIngredientFactory.createClam().toString(), "Fresh Clams from Long Island Sound");
    }
}