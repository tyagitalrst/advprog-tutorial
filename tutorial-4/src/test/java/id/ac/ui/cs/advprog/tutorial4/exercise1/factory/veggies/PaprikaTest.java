package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Paprika;
import org.junit.Before;
import org.junit.Test;

public class PaprikaTest {
	private Paprika paprika;

	@Before
	public void setUp() {
		paprika = new Paprika();
	}

	@Test
    public void testToString() {
    	assertEquals("Paprika", paprika.toString());
    }
}