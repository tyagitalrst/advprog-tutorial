package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
    static Singleton objSingleton;
    // TODO Implement me!
    // What's missing in this Singleton declaration?

    private Singleton() {
        objSingleton = null;
    }

    public static Singleton getInstance() {
        // TODO Implement me!
        if (objSingleton == null) {
            objSingleton = new Singleton();
        }
        return objSingleton;
    }
    
}
