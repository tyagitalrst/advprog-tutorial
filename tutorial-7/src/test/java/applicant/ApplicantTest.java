package applicant;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import java.util.function.Predicate;
import org.junit.Before;
import org.junit.Test;



public class ApplicantTest {
    // TODO Implement me!
    // Increase code coverage in Applicant class
    // by creating unit test(s)!
    private static Applicant applicant;
    private static Predicate<Applicant> credit;
    private  static OutputStream outputStream;

    @Before
    public void setUp() {
        applicant = new Applicant();
        outputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(outputStream);
        System.setOut(ps);
        credit = theAplicant -> theAplicant.getCreditScore() > 600;

    }

    @Test
    public void testCredible() {
        assertTrue(applicant.isCredible());
    }

    @Test
    public void testGetCreditStore() {
        assertEquals(applicant.getCreditScore(), 700);
    }

    @Test
    public void testGetEmployeementYears() {
        assertEquals(applicant.getEmploymentYears(), 10);
    }

    @Test
    public void testGetCriminalRecords() {
        assertTrue(applicant.hasCriminalRecord());
    }

    @Test
    public void testEvaluate() {
        Applicant.evaluate(applicant, credit);
        assertEquals("Result of evaluating applicant: accepted\n", outputStream.toString());
    }
}
